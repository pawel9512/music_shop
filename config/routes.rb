Rails.application.routes.draw do
  concern :statusable do
    resources :order_statuses, shallow: true
  end
  resources :line_items, only: [:create, :update, :destroy]
  resources :carts
  resources :instruments
  resources :categories, only: [:show, :index]
  resources :orders, concerns: :statusable
  resources :order_instruents, only: [], concerns: :statusable
  namespace :store do
    get :sales
    get '/sales/:id', action: :sale, as: :sale
    get :instruments
    get :orders, to: '/orders#index'
  end
  devise_for :users, controllers: {
      registrations: 'registrations',
      sessions: 'sessions'
  }
  root 'instruments#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
