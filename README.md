# RubyonRails Music shop

This project is continuation of a exising application from repository: https://github.com/justalever/flanger.  

### Originally the app provides following functionalities along with presentation layer:  

* Creating an account
* Authentication
* Adding instrument for sale
* Previewing all instruments that are being sold
* Previewing details of a specific instrument
* Cart and ability to add an instrument to cart  

Assumption of this project was to develop further functionalities, which are required in most online stores.

### Features that have been already developed in this project :  

* Database changed to PostgreSql.
* Added instrument`s categories. User can specify tree of a categories that could be assigned to instrument.
* Filtering instruments by categories.
* Orginally cart wasnt assigned to the account. Cart was only assigned to session_id. Now Cart is assigned to an account too.
* Added quantities to instruments, so that user can't add to cart more instruments than are available in stock.
* Added ability to make an order of products that are in the cart.  
	*  While making an order user can choose a delivery option, delivery options are stored in separated table.
	* When order is made, status of an order is also created. Status is separated table. Status has polymorphic association, can be assigned to order or orders_instruments, because orderd instrument also can has a status.
* User can view his orders, and details about the order.
* User can view instruments that he sells.
* User can see details about orders which includes his instruments.
* Test for models. To create test there were used Rspec, FactoryBot, Faker gems.  
* Add cancancan authorization.

### Features that will be provided in the future :  
* Add dotpay payments.
* Add google authentication.
* Some improvments with order statuses.
* Account's wallet management

## Gems and frameworks that was used in the project: 

 * Ancestry
 * Devise
 * Bulma
 * Rspec
 * FactoryBot
 * SimpleForm
 * Draper (Decorator pattern)
 * Pagy (Pagination for indexes)
 * CanCanCan


