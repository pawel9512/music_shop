module ApplicationHelper
  include Pagy::Frontend

  def cart_count_over_one
    return total_cart_items if total_cart_items > 0
  end

  def total_cart_items
    total = @cart.line_items.map(&:quantity).sum
    return total if total > 0
  end

  def user_name_email(user)
    "#{user.name} (#{user.email})"
  end


  def build_menu
    MenuDropDownBuilder.new.build_menu
  end

  def display_alert(error)
    if error.kind_of? Array
      content_tag(:ul) do
        error.each do |msg|
          concat(content_tag(:li, msg))
        end
      end
    else
      error
    end
  end

  class MenuDropDownBuilder
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::UrlHelper
    include ActionView::Context

    def build_menu
      html = ''
      Category.roots.each do |category|
        html += build_first_level_nav(category)
      end
      html.html_safe
    end

    private

    def build_first_level_nav(category)
      if category.children.empty?
        a_class = category.root? ? 'navbar-item' : 'dropdown-item'
        return link_to category.name, Rails.application.routes.url_helpers.category_path(category), class: a_class
      end
      content_tag(:div, class: 'nested dropdown-item dropdown') do
        build_button_nav category
        concat(build_second_level_nav category)
      end
    end

    def build_second_level_nav(category)
      content_tag(:div, class: 'dropdown-menu', id: 'dropdown-menu', role: 'menu') do
        concat(content_tag(:div, class: 'dropdown-content') do
          category.children.each do |children|
            concat(build_first_level_nav children)
          end
        end
        )
      end
    end

    def build_button_nav(category)
      concat(content_tag(:div, class: 'dropdown-trigger') do
        content_tag(:button, class: 'button', aria_haspopup: 'true', aria_controls: 'dropdown-menu') do
          concat(link_to(category.name, Rails.application.routes.url_helpers.category_path(category)))
          concat(content_tag(:span, class: 'icon is-small') do
          end)
        end
      end)
    end
  end

end
