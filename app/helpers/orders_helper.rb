module OrdersHelper

  def delivery_price(price)
    return 'Free' if price.blank? or price.zero?
    "#{price}$"
  end

  def address_details(order)
    capture do
      concat content_tag(:p, order.person_name)
      concat content_tag(:p, order.ship_address)
      concat content_tag(:p, order.ship_postal_code + ' ' + order.ship_city)
      concat content_tag(:p, "Phone: #{order.ship_phone}")
    end
  end


end
