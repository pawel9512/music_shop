class CategoriesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_category, only:[:show]

  def show
    @pagy, @instruments = pagy(Instrument.not_sold.includes(:user).where(category: @category.subtree).order("created_at desc"),items: 10)

  end

  def index

  end

  private

  def set_category
    @category = Category.find(params[:id])
  end
end
