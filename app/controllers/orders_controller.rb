class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_order, only: [:destroy, :show]
  decorates_assigned :order
  authorize_resource

  def new
    @order = Order.new
  end

  def create
    order = current_user.orders.new(order_params)
    order.build_order_from_cart(@cart)
    if order.save
      @cart.clear_items
      redirect_to orders_path, notice: 'Order was successfully created.'
    else
      redirect_to new_order_path, alert: order.errors.full_messages
    end
  end

  def index
    @orders = current_user.orders.includes(orders_instruments: :instrument).preload(:order_statuses).order(created_at: :desc)
    @orders = @orders.by_status(params[:status]) if params.key?(:status)
    @pagy, @orders = pagy_countless(@orders.decorate, items: 20)
  end

  def show

  end

  private
  def order_params
    params.require(:order).permit(:first_name, :last_name, :ship_addres,
                                  :ship_postal_code, :ship_city, :ship_voivodeship,
                                  :ship_country, :ship_phone, :details, :delivery_option_id)
  end

  def find_order
    @order = Order.includes(orders_instruments: :instrument).find(params[:id])
  end

end
