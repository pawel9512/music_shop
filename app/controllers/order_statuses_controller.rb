class OrderStatusesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_order_status

  def create
    @statusable.order_statuses.create!(order_status_params)
  end

  def update
    @order_status.update!(order_status_params)
    respond_to do |format|
      format.js
    end
  end

  private
  def find_order_status
    @order_status = OrderStatus.find(params[:id])
  end

  def find_statusable
    @statusable = if params[:order_instrument_id]
                    OrderInstrument.find(params[:order_instrument_id])
                  elsif params[:order_id]
                    Order.find(params[:order_id])
                  end
  end

  def order_status_params
    param = params[:order_status]
    param[:status] = param[:status].to_i if param[:status]
    params.require(:order_status).permit(:status)
  end

end
