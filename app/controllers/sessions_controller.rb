class SessionsController < Devise::SessionsController

  def create
    super
    cart_from_guest_to_user
  end

end
