class StoreController < ApplicationController
  before_action :authenticate_user!
  before_action :find_order, only: [:sale]

  def sales
    authorize! :sales, :store
    @orders = Order.includes(orders_instruments: {instrument: :user}).preload(:order_statuses)
                  .where(users: {id: current_user.id})
                  .order(created_at: :desc)
    @orders = @orders.by_status(params[:status]) if params.key?(:status)
    @pagy, @orders = pagy_countless(@orders.decorate, items: 20)

  end

  def sale
    authorize! :sale, :store, @order
    @order = @order.decorate
  end

  def instruments
    authorize! :instruments, :store
    @pagy, @instruments = pagy_countless(current_user.instruments, items: 20)

  end


  private
  def find_order
    @order = Order.includes(orders_instruments: {instrument: :user}).find(params[:id])
  end
end
