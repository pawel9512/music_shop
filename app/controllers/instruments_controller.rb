class InstrumentsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!, except: [:index, :show]


  def index
    # @instruments = Instrument.not_sold.includes(:user).order("created_at desc")
    @pagy, @instruments = pagy(Instrument.not_sold.includes(:user).order("created_at desc"), items: 10)
  end


  def show
  end

  def new
    @instrument = current_user.instruments.build
  end

  def edit
  end

  def create
    @instrument = current_user.instruments.build(instrument_params)

    respond_to do |format|
      if @instrument.save
        format.html {redirect_to @instrument, notice: 'Instrument was successfully created.'}
        format.json {render :show, status: :created, location: @instrument}
      else
        format.html {render :new}
        format.json {render json: @instrument.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @instrument.update(instrument_params)
        format.html {redirect_to @instrument, notice: 'Instrument was successfully updated.'}
        format.json {render :show, status: :ok, location: @instrument}
      else
        format.html {render :edit}
        format.json {render json: @instrument.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @instrument.destroy
    respond_to do |format|
      format.html {redirect_to instruments_url, notice: 'Instrument was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  def instrument_params
    params.require(:instrument).permit(:brand, :model, :description, :condition, :finish, :title, :price, :image, :category_id)
  end
end
