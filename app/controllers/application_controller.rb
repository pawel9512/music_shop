class ApplicationController < ActionController::Base
  include Pagy::Backend
  protect_from_forgery with: :exception
  include CurrentCart
  before_action :set_cart

  def current_ability
    @current_ability ||= Abilities::Ability.new(current_user, session)
  end

end
