class OrderStatusDecorator < Draper::Decorator
  delegate_all

  def date_reported_local
    object.date_reported.localtime.strftime('%F %H:%M')
  end
end
