class OrdersInstrumentDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end
  #
  def instrument_truncated_title
    object.instrument.title.truncate(70)
  end

  def mini_thumb
    h.image_tag(object.instrument.image_url(:mini_thumb))
  end
end
