class OrderDecorator < Draper::Decorator
  decorates_association :orders_instruments
  decorates_association :order_statuses
  delegate_all
  delegate :count

  def user_owned_orders_instrument(user)
    object.orders_instruments
  end

  def created_at_local
    object.created_at.localtime.strftime('%F %H:%M')
  end

  def user_owned_instruments_quantity(user)
    object.orders_instruments.sum(&:quantity)
  end

  def recent_status
    OrderStatus.get_status object.status
  end

  def delivery_method
    "#{object.delivery_option.name} (#{object.delivery_option.price}$)"
  end

end
