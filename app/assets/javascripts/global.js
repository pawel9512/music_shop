document.addEventListener("turbolinks:load", function () {

    var notification = document.querySelector('.global-notification');

    if (notification) {
        window.setTimeout(function () {
            notification.style.display = "none";
        }, 4000);
    }
    initDropdowns();
});

function initDropdowns() {
    const dropdowns = document.querySelectorAll('.dropdown:not(.nested)');
    for (var i = 0; i < dropdowns.length; i++) {
        var dropdown = dropdowns[i];
        dropdown.addEventListener('click', function (event) {
            event.stopPropagation();
            dropdown.classList.toggle('is-active');
        });
    }
}
