document.addEventListener('turbolinks:load', function () {
    var totalPriceElement = document.getElementById('order-total-price');
    var orderDeliverySummary = document.getElementById('order-delivery-summary');
    if (totalPriceElement) {
        var totalPrice = parseFloat(totalPriceElement.getAttribute('data-total-price'));
    }
    var option = document.querySelector('.order-delivery-option:checked');
    if (option != null) {
        var deliveryPrice = getDeliveryPrice(option);
        totalPriceElement.innerHTML = 'Total price: ' + (totalPrice + deliveryPrice);
        orderDeliverySummary.innerHTML = deliveryPrice + ' $';
    }

    updateTotalOrderPriceListener(totalPriceElement, totalPrice);

    function updateTotalOrderPriceListener(totalPriceElement, totalPrice) {
        var radios = document.getElementsByClassName('order-delivery-option');
        for (var i = 0; i < radios.length; i++) {
            var radio = radios.item(i);
            radio.addEventListener('change', function (e) {
                var deliveryPrice = getDeliveryPrice(e.target);
                totalPriceElement.innerHTML = 'Total price: ' + (totalPrice + deliveryPrice) + ' $';
                orderDeliverySummary.innerHTML = deliveryPrice + ' $';
            })

        }
    }

    function getDeliveryPrice(htmlElement) {
        var deliveryPrice = htmlElement.closest('tr').getElementsByClassName('order-delivery-price')[0].getAttribute('data-delivery-price')
        return parseFloat(deliveryPrice);
    }
});