class Category < ApplicationRecord
  has_ancestry
  mount_uploader :image, ImageUploader
  has_many :instruments
end
