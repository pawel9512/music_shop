class OrderStatus < ApplicationRecord
  after_update :update_statusable
  belongs_to :statusable, polymorphic: true
  scope :recent, -> {order(:created_at)}
  enum status: [:new_order, :paid, :sent, :canceled, :done]

  STATUS_NAMES = {
      new_order: 'New order',
      paid: 'Order paid',
      sent: 'In devliery',
      done: 'Order complete',
      canceled: 'Order canceled'
  }

  def self.orders_instrument_statuses
    self.statuses.slice(:new_order, :sent, :canceled)
  end

  def status_integer
    self.class.statuses[self.status]
  end

  def status_name
    STATUS_NAMES[self.status.to_sym]
  end

  def self.get_status(status)
    if status.is_a? Numeric
      STATUS_NAMES[self.statuses.key(status).to_sym]
    else
      STATUS_NAMES[status.to_sym]

    end
  end

  private

  def update_statusable
    return if self.statusable.class != OrdersInstrument
    order = self.statusable.order
    if_all_items_sent = order.orders_instruments.all? {|o| o.order_status.status.to_sym == :sent}
    if if_all_items_sent and order.order_statuses.first.status.to_sym != :done
      order.order_statuses.create!(status: :done, date_reported: Time.now)
    end
  end


end
