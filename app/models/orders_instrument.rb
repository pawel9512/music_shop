class OrdersInstrument < ApplicationRecord
  before_create :build_status
  belongs_to :instrument
  belongs_to :order
  # has_many :order_statuses, -> {order("date_reported desc")}, as: :statusable, dependent: :destroy
  has_one :order_status, class_name: "OrderStatus", as: :statusable, dependent: :destroy
  scope :by_instrument_owner, ->(user_id) {joins(instrument: :user).where(users: {id: user_id})}

  def total_price
    instrument.price * quantity.to_i
  end

  def build_status
    self.build_order_status({status: :new_order, date_reported: Time.now})
  end

  def status
    order_status.status_name
  end
end
