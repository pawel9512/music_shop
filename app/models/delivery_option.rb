class DeliveryOption < ApplicationRecord
  has_many :orders

  validates :name, :price, presence: true
  validates :price, length: {maximum: 7}

end
