class LineItem < ApplicationRecord
  belongs_to :instrument
  belongs_to :cart
  validate :instrument_must_be_in_stock, if: :quantity_changed?

  scope :not_owned_by_user, -> (user_id) {includes(:instrument).where.not(instruments: {user_id: user_id})}

  def total_price
    instrument.price * quantity.to_i
  end

  def instrument_must_be_in_stock
    errors.add(:base, "Can't add instrument to cart, is out of the stock") if (instrument.quantity.to_i - quantity.to_i).negative?
  end
end
