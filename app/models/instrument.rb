class Instrument < ApplicationRecord
  before_destroy :not_refereced_by_any_line_item
  belongs_to :user, optional: true
  belongs_to :category
  has_many :line_items
  has_many :orders_instruments

  mount_uploader :image, ImageUploader
  serialize :image, JSON # If you use SQLite, add this line
  scope :not_sold, -> {where.not(instruments: {quantity: 0})}
  validates :title, :brand, :price, :model, presence: true
  validates :description, length: {maximum: 1000, too_long: "%{count} characters is the maximum aloud. "}
  validates :title, length: {maximum: 140, too_long: "%{count} characters is the maximum aloud. "}
  validates :price, length: {maximum: 7}

  BRAND = %w{ Fender Gibson Epiphone ESP Martin Dean Taylor Jackson PRS  Ibanez Charvel Washburn }.freeze
  FINISH = %w{ Black White Navy Blue Red Clear Satin Yellow Seafoam }.freeze
  CONDITION = %w{ New Excellent Mint Used Fair Poor }.freeze

  private

  def not_refereced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "Line items present")
      throw :abort
    end
  end

end
