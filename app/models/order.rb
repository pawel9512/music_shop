class Order < ApplicationRecord
  # before_create :set_total_price
  after_create :complete_creation
  after_commit :complete_orders_instrument_creation, on: :create

  belongs_to :user
  belongs_to :delivery_option
  has_many :orders_instruments, dependent: :destroy
  has_many :order_statuses, -> {order("date_reported desc")}, as: :statusable, dependent: :destroy
  accepts_nested_attributes_for :orders_instruments
  validate :check_if_instruments_in_stock?, on: :create

  scope :by_status, -> (status) {joins(:order_statuses).where("order_statuses.status = ? AND order_statuses.date_reported = (SELECT MAX(order_statuses.date_reported) FROM order_statuses WHERE order_statuses.statusable_type = 'Order' AND order_statuses.statusable_id = orders.id)", status.is_a?(Integer) ? status : OrderStatus.statuses[status.to_sym])}
  scope :by_instrument_owner, -> (user) {where(id: user.orders_instruments.distinct.pluck(:order_id))}

  def build_order_from_cart(cart)
    cart.line_items.each do |item|
      instrument = item.instrument
      self.orders_instruments.build({instrument: instrument, quantity: item.quantity})
    end
  end

  def total_price
    orders_instruments.to_a.sum {|item| item.total_price}
  end

  def person_name
    "#{first_name} #{last_name}"
  end

  def instruments_quantity
    orders_instruments.sum(&:quantity)
  end

  def status
    order_statuses.first.try :status_name
  end

  private

  def complete_creation
    order_statuses.build({status: :new_order, date_reported: Time.now})
    self.total_amount = total_price + delivery_option.price
  end

  def complete_orders_instrument_creation
    orders_instruments.each do |o_instrument|
      ActiveRecord::Base.transaction do
        if o_instrument.instrument.quantity >= o_instrument.quantity
          o_instrument.instrument.quantity -= o_instrument.quantity
          o_instrument.instrument.save!
        else

          o_instrument.order_status.status = :canceled
          o_instrument.save!
        end
      end

    end
  end

  def check_if_instruments_in_stock?
    orders_instruments.each do |o_instrument|
      instrument = o_instrument.instrument
      self.errors.add(:base, "#{instrument.title.truncate(50)} is run out of stock") if instrument.quantity < o_instrument.quantity
    end

  end

end
