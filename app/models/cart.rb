class Cart < ApplicationRecord
  has_many :line_items, dependent: :destroy
  belongs_to :user, optional: true

  def add_instrument(instrument, quantity = 1)
    current_item = line_items.find_by(instrument_id: instrument.id)
    if current_item
      current_item.quantity += quantity
    else
      current_item = line_items.build(instrument_id: instrument.id, quantity: quantity)
    end
    current_item
  end

  def total_price
    line_items.to_a.sum {|item| item.total_price}
  end

  def instruments_quantity
    line_items.sum(&:quantity)
  end

  def clear_items
    line_items.destroy_all
  end

end
