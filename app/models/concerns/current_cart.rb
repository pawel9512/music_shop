module CurrentCart

  private

  def set_cart
    if current_user
      if current_user.cart.present?
        @cart = current_user.cart
      else
        create_new_cart(current_user)
      end
      return
    end
    @cart = Cart.find_by(id: session[:cart_id])
    create_new_cart if @cart.nil?
  end

  def create_new_cart(user = nil)
    @cart = Cart.create(user: user)
    session[:cart_id] = @cart.id
    @cart
  end

  def cart_from_guest_to_user
    cart = Cart.find session[:cart_id]
    user_cart = current_user.cart
    return if cart == user_cart
    session[:cart_id] = user_cart.id
    items = cart.line_items
    if items.count > 0
      items.each do |item|
        user_cart.add_instrument(item.instrument).save! unless user_cart.line_items.find_by(instrument: item.instrument).present?
      end
    end
    cart.destroy!
  end
end