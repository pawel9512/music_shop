module Abilities
  module Default

    def default_user_abilities
      can [:index, :show], Instrument
      can [:update, :destroy], Instrument, user_id: @user.id

      can :create, LineItem
      can [:update, :destroy], LineItem, cart_id: @user.cart.id

      can [:show, :update, :destroy], Cart, id: @user.cart.id

      can [:new, :create, :index], Order
      can :show, Order, user_id: @user.id

      can [:sales, :instruments], :store
      can :sale, :store do |symbol, order|
        order.orders_instruments.any? do |order_instrument|
          order_instrument.instrument.user == @user
        end
      end
    end

  end
end