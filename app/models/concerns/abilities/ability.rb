module Abilities
  class Ability

    include CanCan::Ability
    include Abilities::Default
    include Abilities::Admin
    include Abilities::Guest

    def initialize(user, session)
      @user = user
      if user.blank?
        guest_user_abilities(session[:cart_id])
        return
      end
      if user.admin?
        admin_user_abilities
      else
        default_user_abilities
      end
    end


  end
end