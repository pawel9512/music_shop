module Abilities
  module Admin

    def admin_user_abilities
      can [:index, :show], Instrument
    end

  end
end