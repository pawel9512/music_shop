module Abilities
  module Guest

    def guest_user_abilities(cart_id)

      can [:index, :show], Instrument

      can :create, LineItem
      can [:update, :destroy], LineItem, cart_id: cart_id

      can [:show, :update, :destroy], Cart, id: cart_id

    end

  end
end