require 'rails_helper'
require 'cancan/matchers'

RSpec.describe LineItem, type: :model do

  it 'has valid facctory' do
    expect(build(:line_item)).to be_valid
  end

  describe 'associations' do
    subject {create(:line_item)}
    it {is_expected.to belong_to(:instrument)}
    it {is_expected.to belong_to(:cart)}
  end

  describe 'validations' do

    describe 'instrument_must_be_in_stock' do
      let(:instrument) {create(:instrument)}
      let(:cart) {create(:cart)}

      context 'when instrument is out of the stock' do
        it 'line item is invalid' do
          instrument.quantity = 1
          line_item = cart.add_instrument(instrument, 2)
          line_item.valid?
          expect(line_item.errors[:base].first).to eq("Can't add instrument to cart, is out of the stock")
        end
      end

      context 'when instrument is in stock' do
        it 'line item is valid' do
          instrument.quantity = 2
          line_item = cart.add_instrument(instrument, 1)
          line_item.valid?
          expect(line_item).to be_valid
        end
      end
    end
  end

  describe 'abilities' do
    subject(:ability) {Abilities::Ability.new(user, {cart_id: cart_id})}
    let(:user) {nil}
    let(:cart_id) {20}

    context 'when guest user' do
      let(:user_item) {build(:line_item, cart_id: cart_id)}
      let(:not_user_item) {build(:line_item)}

      it {is_expected.to be_able_to(:create, LineItem)}
      it {is_expected.to be_able_to(:update, user_item)}
      it {is_expected.not_to be_able_to(:update, not_user_item)}
      it {is_expected.to be_able_to(:destroy, user_item)}
      it {is_expected.not_to be_able_to(:destroy, not_user_item)}
    end

    context 'when default user' do
      let(:user) {create(:user)}
      let(:user_item) {build(:line_item, cart_id: user.cart.id)}
      let(:not_user_item) {build(:line_item)}

      it {is_expected.to be_able_to(:create, LineItem)}
      it {is_expected.to be_able_to(:update, user_item)}
      it {is_expected.not_to be_able_to(:update, not_user_item)}
      it {is_expected.to be_able_to(:destroy, user_item)}
      it {is_expected.not_to be_able_to(:destroy, not_user_item)}

    end

  end

  describe '.total_price' do
    it 'returns total price' do
      quantity = 2
      price = 2321.23
      item = build(:line_item, quantity: quantity, instrument: build(:instrument, price: price))
      expect(item.total_price).to eq(price * quantity)
    end
  end


end
