require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has valid factory' do
    expect(build(:user)).to be_valid
  end

  describe 'associations' do
    it {is_expected.to have_many(:instruments)}
    it {is_expected.to have_many(:orders)}
    it {is_expected.to have_many(:orders_instruments)}
    it {is_expected.to have_one(:cart)}
  end
end
