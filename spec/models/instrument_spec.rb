require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Instrument, type: :model do

  it ' has valid factory' do
    expect(build(:instrument)).to be_valid
  end

  describe 'Associations' do
    it {is_expected.to belong_to(:category)}
    it {is_expected.to have_many(:line_items)}
    it {is_expected.to have_many(:orders_instruments)}
  end

  describe 'validations' do
    subject {build(:instrument)}

    describe 'title' do
      it {is_expected.to validate_presence_of(:title)}
      it {is_expected.to validate_length_of(:title).is_at_most(140).with_message(/140 characters is the maximum aloud. /)}

      context 'when title is blank' do
        it 'is invalid' do
          subject.title = ''
          expect(subject).to_not be_valid
        end
      end

      context 'when title has more than 140 letters' do
        it 'is invalid' do
          subject.title = (0..141).map {'r'}.join
          expect(subject).to_not be_valid
        end

      end
    end

    describe 'price' do
      it {is_expected.to validate_presence_of(:price)}
      it 'is not valid without price' do
        subject.price = nil
        expect(subject).to_not be_valid
      end
    end

    describe 'model' do
      it {is_expected.to validate_presence_of(:model)}
      it 'is not valid without model' do
        subject.model = nil
        expect(subject).to_not be_valid
      end
    end

    describe 'brand' do
      it {is_expected.to validate_presence_of(:brand)}
      it 'is not valid without brand' do
        subject.brand = nil
        expect(subject).to_not be_valid
      end
    end

    describe 'description' do
      it {is_expected.to validate_length_of(:description).is_at_most(1000).with_message(/1000 characters is the maximum aloud. /)}

      context 'when description is blank' do
        it 'is valid' do
          subject.description = ''
          expect(subject).to be_valid
        end
      end

      context 'when description is above 1000 letters' do
        it 'is invalid' do
          subject.description = (0..1001).map {'r'}.join
          expect(subject).not_to be_valid
        end
      end
    end
  end

  describe 'abilities' do
    subject(:ability) {Abilities::Ability.new(user, {cart_id: 4})}
    let(:user) {nil}

    context 'when guest user' do
      it {is_expected.to be_able_to(:index, Instrument)}
      it {is_expected.to be_able_to(:show, Instrument.new)}
      it {is_expected.not_to be_able_to(:update, Instrument.new)}
      it {is_expected.not_to be_able_to(:destroy, Instrument.new)}
    end

    context 'when default user' do
      let(:user) {create(:user)}
      let(:user_instrument) {create(:instrument, user: user)}

      it {is_expected.to be_able_to(:index, Instrument)}
      it {is_expected.to be_able_to(:show, Instrument.new)}
      it {is_expected.to be_able_to(:update, user_instrument)}
      it {is_expected.to be_able_to(:destroy, user_instrument)}
      it {is_expected.not_to be_able_to(:update, Instrument.new)}
      it {is_expected.not_to be_able_to(:destroy, Instrument.new)}
    end
  end

  describe '#destroy' do

    let(:instrument) {create(:instrument)}
    let (:cart) {create(:cart)}

    it 'run callback' do
      expect(instrument).to receive(:not_refereced_by_any_line_item)
      instrument.run_callbacks(:destroy)
    end

    context 'when referenced by line items' do
      it 'throws error' do
        cart.line_items << create(:line_item, instrument: instrument)
        instrument.destroy
        expect(instrument.errors[:base].first).to eq('Line items present')
      end
    end
  end
end
