require 'rails_helper'

RSpec.describe DeliveryOption, type: :model do

  describe 'assocations' do
    it {is_expected.to have_many :orders}
  end

  describe 'validations' do
    subject {build(:delivery_option)}

    describe 'price' do
      it {is_expected.to validate_presence_of(:price)}

      context 'when price is blank' do
        it 'is invalid' do
          subject.price = nil
          subject.valid?
          expect(subject.errors[:price].first).to eq("can't be blank")
        end
      end
    end

    describe 'name' do
      it {is_expected.to validate_presence_of(:name)}

      context 'when name is blank' do
        it 'is invalid' do
          subject.name = nil
          subject.valid?
          expect(subject.errors[:name].first).to eq("can't be blank")

        end
      end

    end
  end
end
