require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Cart, type: :model do

  it 'has valid factory' do
    expect(build(:cart)).to be_valid
  end

  describe 'associations' do
    it {is_expected.to belong_to(:user).optional}
    it {is_expected.to have_many(:line_items)}
  end

  describe 'abilities' do
    subject(:ability) {Abilities::Ability.new(user, {cart_id: cart.id})}
    let(:user) {nil}
    let(:cart) {build(:cart)}

    context 'when guest user' do
      let (:not_user_cart) {create(:cart)}

      it {is_expected.to be_able_to(:show, cart)}
      it {is_expected.not_to be_able_to(:show, not_user_cart)}
      it {is_expected.to be_able_to(:update, cart)}
      it {is_expected.not_to be_able_to(:update, not_user_cart)}
      it {is_expected.to be_able_to(:destroy, cart)}
      it {is_expected.not_to be_able_to(:destroy, not_user_cart)}
    end

    context 'when default user' do
      let(:user) {create(:user)}
      let (:not_user_cart) {create(:cart)}

      it {is_expected.to be_able_to(:show, user.cart)}
      it {is_expected.not_to be_able_to(:show, not_user_cart)}
      it {is_expected.to be_able_to(:update, user.cart)}
      it {is_expected.not_to be_able_to(:update, not_user_cart)}
      it {is_expected.to be_able_to(:destroy, user.cart)}
      it {is_expected.not_to be_able_to(:destroy, not_user_cart)}
    end


  end


  describe '.add_instrument' do
    let(:instrument) {create(:instrument)}
    let(:cart) {create(:cart)}

    context 'when instrument is out of the stock' do
      it 'return line_item object' do
        instrument.quantity = 1
        line_item = cart.add_instrument(instrument, 2)
        expect(line_item).to be_a LineItem
      end
    end

    context 'when instrument is in stock' do
      it 'return line_item object' do
        instrument.quantity = 2
        line_item = cart.add_instrument(instrument, 1)
        expect(line_item).to be_a LineItem
      end
    end

  end

  describe '.total_price' do

    it 'returns valid price' do
      cart = build(:cart_with_line_items)
      prices = [2000.12, 543.23, 832.12]
      quantities = [2, 1, 3]
      total_price = 0
      prices.each_with_index do |p, i|
        total_price += p * quantities[i]
        cart.line_items[i].instrument.price = p
        cart.line_items[i].quantity = quantities[i]
      end
      expect(cart.total_price).to eq(total_price)
    end

  end

  describe '.instruments_quantity' do
    it 'returns cart instrumnets quantity' do
      cart = build(:cart_with_line_items)
      quantities = [4, 5, 3]
      quantities.each_with_index do |q, i|
        cart.line_items[i].quantity = q
      end
      expect(cart.instruments_quantity).to eq(quantities.sum)
    end
  end

  describe '.clear_items' do
    it 'destroys all line_items' do
      cart = build(:cart_with_line_items)
      expect {cart.clear_items}.to change {LineItem.count}.by(-3)
    end
  end
end
