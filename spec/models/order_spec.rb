require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Order, type: :model do

  describe 'associations' do
    it {is_expected.to belong_to :user}
    it {is_expected.to belong_to :delivery_option}
    it {is_expected.to have_many :orders_instruments}
    it {is_expected.to have_many :order_statuses}
  end

  describe 'validations' do

    describe 'check_if_instruments_in_stock?' do
      let(:order) {build(:order)}
      let(:instrument) {build(:instrument)}

      context 'when instruments quantity is less than ordered quantity' do
        it 'order is not valid' do
          instrument.quantity = 2
          order.orders_instruments << build(:orders_instrument, order: order, instrument: instrument, quantity: 4)
          order.valid?
          expect(order.errors[:base]).to include("#{instrument.title.truncate(50)} is run out of stock")
        end
      end

      context 'when instruments quantity is grater than ordered quantity' do
        it 'order is valid' do
          instrument.quantity = 4
          order.orders_instruments << build(:orders_instrument, order: order, instrument: instrument, quantity: 4)
          expect(order).to be_valid
        end
      end
    end
  end

  describe 'abilities' do
    subject(:ability) {Abilities::Ability.new(user, {cart_id: cart_id})}
    let(:user) {nil}
    let(:cart_id) {20}

    context 'when guest user' do
      let(:order) {create(:order)}

      it {is_expected.not_to be_able_to(:create, Order)}
      it {is_expected.not_to be_able_to(:index, Order)}
      it {is_expected.not_to be_able_to(:show, order)}

      it {is_expected.not_to be_able_to(:sales, :store)}
      it {is_expected.not_to be_able_to(:instruments, :store)}

    end

    context 'when default user' do
      let(:user) {create(:user)}
      let(:user_order) {create(:order, user: user)}
      let(:not_user_order) {create(:order)}

      it {is_expected.to be_able_to(:create, Order)}
      it {is_expected.to be_able_to(:index, Order)}
      it {is_expected.to be_able_to(:show, user_order)}
      it {is_expected.not_to be_able_to(:show, not_user_order)}

      it {is_expected.to be_able_to(:sales, :store)}
      it {is_expected.to be_able_to(:instruments, :store)}
      it 'has access to sale store' do
        not_user_order.orders_instruments << create(:orders_instrument, order: not_user_order, instrument: create(:instrument, user: user))
        expect(ability).to be_able_to(:sale, :store, not_user_order)
        expect(ability).not_to be_able_to(:sale, :store, Order.new)
      end


    end
  end

  describe '#create' do
    subject {build(:order)}

    it 'runs complete_creation callback' do
      expect(subject).to receive(:complete_creation)
      subject.run_callbacks(:create)
    end

    it 'creates order status and sets order total_amount' do
      prices = [432.00, 5432.53, 53.03]
      delivery_price = 12.01
      quantity = 2
      total_price = (prices.sum {|p| p * quantity}) + delivery_price
      subject.orders_instruments = prices.map {|price| build(:orders_instrument, quantity: quantity, order: subject, instrument: create(:instrument, price: price, quantity: quantity))}
      subject.delivery_option.price = delivery_price
      expect {subject.save!}.to change {OrderStatus.where(statusable_type: 'Order').count}.by(1)
      expect(subject.total_amount).to eq(total_price)
    end


    it 'if instrument run out of stack change orders_instrument status to canceled' do
      order = build(:order)
      instrument = build(:instrument, quantity: 1)
      order.orders_instruments << build(:orders_instrument, order: order, instrument: instrument, quantity: 2)
      order.save(validate: false)
      expect(order.orders_instruments.first.status).to eq(OrderStatus::STATUS_NAMES[:canceled])
    end

    it 'decreases number of instrument quantity' do
      order = build(:order)
      instrument = build(:instrument, quantity: 2)
      order.orders_instruments << build(:orders_instrument, order: order, instrument: instrument)
      order.save
      expect(instrument.quantity).to eq(1)
    end

  end

  describe '.total_price' do
    it 'returns order instruments total price' do
      order = create(:order_with_instruments)
      prices = [432.00, 5432.53, 53.03]
      quantities = [1, 3, 5]
      total_price = 0
      prices.each_with_index do |p, index|
        order.orders_instruments[index].instrument.price = p
        order.orders_instruments[index].quantity = quantities[index]
        total_price += p * quantities[index]
      end
      expect(order.total_price).to eq(total_price)
    end
  end

  describe '.person_name' do
    it 'returns person full name' do
      order = build(:order, first_name: 'Paweł', last_name: 'Glootie')
      expect(order.person_name).to eq('Paweł Glootie')
    end
  end

  describe '.instruments_quantity' do
    it 'returns ordered instruments quantity' do
      order = build(:order_with_instruments, orders_instruments_count: 5)
      expect(order.instruments_quantity).to eq(5)
    end

    context 'when instruments quantity is zero' do
      it 'returns zero' do
        order = build(:order)
        expect(order.instruments_quantity).to eq(0)
      end

    end
  end

  describe '.status' do
    it 'returns last status name' do
      order = create(:order)
      expect(order.status).to eq(OrderStatus::STATUS_NAMES.first[1])
    end
  end

  describe 'scopes' do

    describe '.by_status' do
      before :all do
        @order1 = create(:order)
        @order2 = create(:order)
        @order3 = create(:order)
        @order3.order_statuses << create(:order_status, statusable: @order3, status: :done)
      end
      context 'when filtering by new_order status' do
        it 'returns orders with new_order status' do
          new_orders = described_class.by_status(:new_order)
          expect(new_orders).to include(@order2, @order1)
          expect(new_orders).not_to include(@order3)
        end
      end
      context 'when filtering by done status' do
        it 'returns orders with done status' do
          done_orders = described_class.by_status(:done)
          expect(done_orders).to include(@order3)
          expect(done_orders).not_to include(@order2, @order1)
        end
      end

    end

    describe '.by_instrument_owner' do
      it 'returns all orders that has specific user instruments' do
        user = create(:user)
        order1 = create(:order)
        order2 = create(:order_with_instruments)
        order1.orders_instruments << create(:orders_instrument, order: order1, instrument: create(:instrument, user: user))
        order1.orders_instruments << create(:orders_instrument, order: order1, instrument: create(:instrument, user: user))
        orders_by_inst_owner = Order.by_instrument_owner(user)
        expect(orders_by_inst_owner).to include(order1)
        expect(orders_by_inst_owner).not_to include(order2)
      end
    end
  end

end
