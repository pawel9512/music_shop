require 'rails_helper'

RSpec.describe OrdersInstrument, type: :model do
  it 'has valid fatory' do
    expect(build(:orders_instrument)).to be_valid
  end

  describe 'associations' do
    it {is_expected.to belong_to(:order)}
    it {is_expected.to belong_to(:instrument)}
    it {is_expected.to have_one(:order_status)}
  end

  describe '.total_price' do
    it 'returns total_pirce of specific instrument' do
      price = 2643
      quantity = 3
      o_instrument = build(:orders_instrument, instrument: build(:instrument, price: price), quantity: quantity)
      expect(o_instrument.total_price).to eq(price * quantity)
    end
  end

  describe '.status' do
    it 'returns orders_instrumnet status' do
      o_instrument = create(:orders_instrument)
      o_instrument.order_status.status = :sent
      expect(o_instrument.status).to eq(OrderStatus::STATUS_NAMES[:sent])
    end
  end

  describe '#create' do
    subject {build(:orders_instrument)}

    it 'runs build_status callback' do
      expect(subject).to receive(:build_status)
      subject.run_callbacks(:create)
    end

    it 'creates order status and decreases instrument quantity' do
      instrument = create(:instrument, quantity: 1)
      subject.instrument = instrument
      subject.quantity = 2
      expect {subject.save!}.to change {OrderStatus.where(statusable_type: 'OrdersInstrument').count}.by(1)
      expect(instrument.quantity).to eq(1)
    end
  end
end
