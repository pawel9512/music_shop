FactoryBot.define do
  factory :order_status do
    status {0}
    date_reported {Time.now}

    trait :for_order do
      association :statusable, factory: :order
    end
    trait :for_instrument do
      association :statusable, factory: :orders_instrument
    end
  end
end
