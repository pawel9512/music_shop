FactoryBot.define do
  factory :instrument do
    title {Faker::App.name}
    brand {Instrument::BRAND.sample}
    price {Faker::Commerce.price range: 200..2000.00}
    model {Faker::App.name}
    description {Faker::Lorem.paragraph(sentence_count: 20)}
    quantity {1}
    association :category, factory: :category_with_descendants

  end
end
