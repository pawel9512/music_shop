FactoryBot.define do
  factory :line_item do
    transient do
      cart_id {21234}
    end
    quantity {1}
    association :instrument, factory: :instrument
    cart {build(:cart, id: cart_id)}
  end
end