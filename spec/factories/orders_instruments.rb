FactoryBot.define do
  factory :orders_instrument do
    association :order, factory: :order
    instrument
    quantity {1}
  end
end
