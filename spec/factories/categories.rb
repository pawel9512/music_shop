FactoryBot.define do
  factory :category do
    name {Faker::App.name}
    factory :category_with_descendants do
      transient do
        descendants_count {3}
      end
      after(:create) do |category, evaluator|
        create_list(:category, evaluator.descendants_count, parent: category)
      end
    end
  end
end
