FactoryBot.define do
  factory :order do
    user
    delivery_option
    factory :order_with_instruments do
      transient do
        orders_instruments_count {3}

      end
      after(:build) do |order, evaluator|
        order.orders_instruments = create_list(:orders_instrument, evaluator.orders_instruments_count)
      end
    end
  end
end
