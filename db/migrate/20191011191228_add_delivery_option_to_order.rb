class AddDeliveryOptionToOrder < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :delivery_option, foreign_key: true
  end
end
