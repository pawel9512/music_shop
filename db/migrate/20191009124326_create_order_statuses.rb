class CreateOrderStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :order_statuses do |t|
      t.references :order, foreign_key: true
      t.integer :status, default: 0
      t.datetime :date_reported
      t.timestamps
    end
  end
end
