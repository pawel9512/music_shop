class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true
      t.decimal :total_amount, precision: 8, scale: 2, default: 0
      t.string :uniq_control
      t.string :first_name
      t.string :last_name
      t.string :details
      t.string :ship_address
      t.string :ship_postal_code
      t.string :ship_city
      t.string :ship_voivodeship
      t.string :ship_country
      t.string :ship_phone
      t.timestamps
    end
  end
end
