class AddCategoryToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_reference :instruments, :category, foreign_key: true
  end
end
