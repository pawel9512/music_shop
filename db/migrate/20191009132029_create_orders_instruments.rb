class CreateOrdersInstruments < ActiveRecord::Migration[5.1]
  def change
    create_table :orders_instruments do |t|
      t.references :instrument, foreign_key: true
      t.references :order, foreign_key: true
      t.integer :quantity
      t.string :comment
      t.decimal :discount

      t.timestamps
    end
  end
end
