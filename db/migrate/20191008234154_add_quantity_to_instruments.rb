class AddQuantityToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_column :instruments, :quantity, :integer
  end
end
