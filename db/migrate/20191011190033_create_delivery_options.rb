class CreateDeliveryOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_options do |t|
      t.string :name
      t.text :details
      t.decimal :price, precision: 8, scale: 2, default: 0

      t.timestamps
    end
  end
end
