class AddPolymorphismToOrderStatus < ActiveRecord::Migration[5.1]
  def change
    remove_index :order_statuses, :order_id
    remove_column :order_statuses, :order_id
    add_reference :order_statuses, :statusable, polymorphic: true, index: true
  end
end
