# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191022192650) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "carts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_carts_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.string "image"
    t.index ["ancestry"], name: "index_categories_on_ancestry"
  end

  create_table "delivery_options", force: :cascade do |t|
    t.string "name"
    t.text "details"
    t.decimal "price", precision: 8, scale: 2, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "instruments", force: :cascade do |t|
    t.string "brand"
    t.string "model"
    t.text "description"
    t.string "condition"
    t.string "finish"
    t.string "title"
    t.decimal "price", precision: 8, scale: 2, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.integer "user_id"
    t.bigint "category_id"
    t.integer "quantity"
    t.index ["category_id"], name: "index_instruments_on_category_id"
  end

  create_table "line_items", force: :cascade do |t|
    t.bigint "instrument_id"
    t.bigint "cart_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity", default: 1
    t.index ["cart_id"], name: "index_line_items_on_cart_id"
    t.index ["instrument_id"], name: "index_line_items_on_instrument_id"
  end

  create_table "order_statuses", force: :cascade do |t|
    t.integer "status", default: 0
    t.datetime "date_reported"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "statusable_type"
    t.bigint "statusable_id"
    t.index ["statusable_type", "statusable_id"], name: "index_order_statuses_on_statusable_type_and_statusable_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.decimal "total_amount", precision: 8, scale: 2, default: "0.0"
    t.string "uniq_control"
    t.string "first_name"
    t.string "last_name"
    t.string "details"
    t.string "ship_address"
    t.string "ship_postal_code"
    t.string "ship_city"
    t.string "ship_voivodeship"
    t.string "ship_country"
    t.string "ship_phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "delivery_option_id"
    t.index ["delivery_option_id"], name: "index_orders_on_delivery_option_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "orders_instruments", force: :cascade do |t|
    t.bigint "instrument_id"
    t.bigint "order_id"
    t.integer "quantity"
    t.string "comment"
    t.decimal "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["instrument_id"], name: "index_orders_instruments_on_instrument_id"
    t.index ["order_id"], name: "index_orders_instruments_on_order_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "role", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "carts", "users"
  add_foreign_key "instruments", "categories"
  add_foreign_key "line_items", "carts"
  add_foreign_key "line_items", "instruments"
  add_foreign_key "orders", "delivery_options"
  add_foreign_key "orders", "users"
  add_foreign_key "orders_instruments", "instruments"
  add_foreign_key "orders_instruments", "orders"
end
